package com.tbuttler.concurrency.model;


/**
 * Compare and swap is a pattern to achieve synchronization for atomic operations in memory.
 * 
 * This class contains two implementations that do not show illegal states with JCStress, and one that should be avoided.
 * 
 * @see Wikipedia - <a href="https://en.wikipedia.org/wiki/Compare-and-swap">Compare and Swap</a>
 * 
 * @author Tanja
 *
 */
public class CompareAndSwap {
	
private int notSave = 0;
private int save = 0;

private volatile int vmSave = 0;
	
	/**
	 * Changes the value of the internal field {@link toInt} IFF the internal field matches the expected value {@link fromInt}.
	 * 
	 * @param fromInt only change the internal field if the stored value matches this variable
	 * @param toInt new value of the internal field, if a 
	 * @return true if the internal field was changed
	 */
	public boolean swapNotThreadSafe (int fromInt, int toInt) {
		boolean result = false;
		if(notSave == fromInt) {
			notSave = toInt;
			result = true;
		}
		return result;
	}
	
	/**
	 * 
	 * Changes the value of the internal field {@link toInt} IFF the internal field matches the expected value {@link fromInt}.
	 * 
	 * This method uses a synchronized block (on this) to control access to the internal field. Should be thread-safe.
	 * 
	 * @param fromInt only change the internal field if the stored value matches this variable
	 * @param toInt new value of the internal field, if a 
	 * @return true if the internal field was changed
	 */
	public boolean swapWithMutex(int fromInt, int toInt) {
		synchronized (this) {
			boolean result = false;
			if(save == fromInt) {
				save = toInt;
				result = true;
			}
			return result;	
		}
	}
	
	/**
	 * Same as {@link CompareAndSwap#swapWithMutex(int, int)}, but declares the internal field as volatile.
	 * For concurrent situations this should result in the same outcome, as a synchronized block treats all variables accessed in that
	 * block as volatile. 
	 */
	public boolean swapVolatileWithMutex(int fromInt, int toInt) {
		synchronized (this) {
			boolean result = false;
			if(vmSave == fromInt) {
				vmSave = toInt;
				result = true;
			}
			return result;	
		}
	}

}
