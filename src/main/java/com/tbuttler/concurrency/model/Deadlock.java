package com.tbuttler.concurrency.model;

/**
 * This class has a high chance of creating a deadlock if threadOne and threadTwo
 * are started in separate threads at the same time.
 * 
 * @author Tanja
 *
 */
public class Deadlock {
	
	private static final int millis = 100;
	private static final Object lock1 = new Object();
	private static final Object lock2 = new Object();
	
	/**
	 * Acquire lock one, then lock two.
	 */
	public void threadOne() {
		synchronized (lock1) {
			sleep();
			synchronized (lock2) {
				sleep();
			}
		}
	}
	
	/**
	 * Acquire lock two, then lock one.
	 */
	public void threadTwo() {
		synchronized(lock2) {
			sleep();
			synchronized(lock1) {
				sleep();
			}
		}
	}

	/**
	 * Sleep for a fixed duration.
	 */
	private void sleep() {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
