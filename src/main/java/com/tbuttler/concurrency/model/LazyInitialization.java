package com.tbuttler.concurrency.model;

/**
 * Features several patterns for initializing an expensive object. These methods mostly create the instance 
 * lazily. 
 *  {@link LazyInitialzation#useClassInitialzation(int)} does not use lazy initialization: the instance does not 
 *  use the parameter.
 * 
 * {@link LazyInitialization#getInstanceWithDoubleNullCheck(int)} and {@link LazyInitialzation#useClassInitialzation(int)}
 * are thread-safe initializations.
 * 
 * @author Tanja
 *
 */

public class LazyInitialization {
	//TODO write the tests for earlyInstance & static creator!
	
	private static final ExpensiveObject earlyInstance = new ExpensiveObject(0);
		
	/**
	 * An "expensive" object that is lazily initialized.
	 * 
	 *  The {@link ExpensiveObject#value} is used to track which instance was actually created.
	 *  
	 * @author Tanja
	 *
	 */
		public static class ExpensiveObject {
			
			private int value;

			ExpensiveObject(int value) {
				this.value = value;
			}
			
			public int getValue() {
				return value;
			}
			
		}
		
		private static class InstanceHolder {
			
			private static ExpensiveObject instance = new ExpensiveObject(0);
			
			private ExpensiveObject getInstance() {
				return instance;
			}
			
		}
	
	    private ExpensiveObject instance = null;
	    private InstanceHolder holder = new InstanceHolder();

	    /**
	     * Simple lazy initialization without any synchronization. The value of the expensive object
	     * is set to the initial value IFF this is the first access. This implementation is not thread-safe
	     * 
	     * @param initialValue to identify who initialized the ExpensiveObject
	     * @return the instance of an ExpensiveObject
	     */
	    public ExpensiveObject getInstanceNotThreadSafe(int initialValue) {
	        if (instance == null)
	            instance = new ExpensiveObject(initialValue);
	        return instance;
	    }
	    
	    /**
	     * Lazy initialization where the test, whether an instance exists, is performed outside
	     * the synchronized part of the code. This implementation is not thread-safe, as the 
	     * check whether the instance exists could rely on cached instances: the instance would be considered
	     * null even if another thread had already initialized it.
	     * 
	     * @param initialValue to identify who initialized the ExpensiveObject
	     * @return the instance of an ExpensiveObject
	     */
	    public ExpensiveObject getInstanceWithBrokenPattern(int initialValue) {
	    	if(instance == null) {
	    		synchronized(this) {
	    			instance = new ExpensiveObject(initialValue);
	    		}
	    	}
	    	return instance;
	    }
	    
	    /**
	     * Checks whether the instance is null within the synchronized block. This implementation
	     * should be thread-safe. 
	     * 
	     * @param initialValue to identify who initialized the ExpensiveObject
	     * @return the instance of an ExpensiveObject
	     */
	    public ExpensiveObject getInstanceWithDoubleNullCheck(int initialValue) {
	    	if(instance == null) {//cheap check
	    		synchronized(this) {
	    			if(instance == null)//expensive check in synchronized block
	    				instance = new ExpensiveObject(initialValue);
	    		}
	    	}
	    	return instance;
	    }
	    
	    /**
	     * This method returns a class variable holding an expensive object.
	     * No synchronization is needed, but the expensive object is not released when no references to
	     * it's holder exist. Also, the (expensive) instance is created when the class LazyInitialization
	     * is created -- so this is not a lazy initialization.
	     * 
	     * @param initialValue cannot be used as the instance is created when the class LazyInitialization
	     * is loaded.
	     * @return instance of an ExpensiveObject created when this class was loaded
	     */
	    public ExpensiveObject useClassInitialization(int initialValue) {
	    	return earlyInstance;
	    }
	    
	    public ExpensiveObject useStaticHolder(int initialValue) {
	    	return holder.getInstance();
	    }
	    
	    
	    
	    //TODO any other thread-safe ways to initialize an instance?
	    
	}
