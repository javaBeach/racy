package com.tbuttler.concurrency.test.compareSwap;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.Expect;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.LongResult2;

import com.tbuttler.concurrency.model.CompareAndSwap;

public class CompareSwap_VolatileMutex {

	@State
	public static class TestedCompareAndSwap extends CompareAndSwap {
	};

	@JCStressTest
	@Description("Actors 1 and 2 race for changing the initial value in a DataRace object.")
	@Outcome(id = "[5, 2]", expect = Expect.ACCEPTABLE, desc = "A1 A2 execution.")
	@Outcome(id = "[1, 10]", expect = Expect.ACCEPTABLE, desc = "A2 A1 execution.")
	public static class ValueSwaps {

		/**
		 * Actor 1 performs a swap to 5.
		 * 
		 * @param swap
		 *            on which the race condition will be tested
		 * @param l
		 *            result value used to store whether actor 1 performed a
		 *            swap.
		 */
		@Actor
		public void actor1(TestedCompareAndSwap swap, LongResult2 l) {
			boolean hasSwap = swap.swapVolatileWithMutex(0, 5);
			// if actor1 is first: set result to 5, else set the actor number
			l.r1 = hasSwap ? 5 : 1;
		}

		/**
		 * 
		 * 
		 * @param swap
		 *            on which the race condition will be tested
		 * @param l
		 *            result value used to store whether actor 2 performed a
		 *            swap.
		 */
		@Actor
		public void actor2(TestedCompareAndSwap swap, LongResult2 l) {
			boolean hasSwap = swap.swapVolatileWithMutex(0, 10);
			// if actor2 is first: set result to 10, else set the actor number
			l.r2 = hasSwap ? 10 : 2;
		}
	}

}
