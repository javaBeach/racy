package com.tbuttler.concurrency.test.deadlock;

import org.openjdk.jcstress.annotations.Actor;

import com.tbuttler.concurrency.model.Deadlock;

/**
 * This test does not compile yet: Mode Termination expects a single actor, who receives a signal
 * and is expected to terminate based on the signal.
 * 
 * I want to run two actors with a test that checks whether both actors terminate.
 * @author Tanja
 *
 */
//@JCStressTest(Mode.Termination)
//@Description("Tests if deadlocks occur (there should be some.")
//@Outcome(id = "TERMINATED", expect = Expect.ACCEPTABLE, desc = "Both threads have finished.")
//@Outcome(id = "STALE", expect = Expect.FORBIDDEN, desc = "A deadlock occurred.")
//@State
public class DeadlockDetectionTest {
	
	final Deadlock deadlock = new Deadlock();
	
	/**
	 * running thread one with the intention of creating a deadlock
	 */
	@Actor
	public void actor1() {
		deadlock.threadOne();
	}
	
	/**
	 * running thread two with the intention of creating a deadlock
	 */
	@Actor
	public void actor2() {
		deadlock.threadTwo();
	}

}
