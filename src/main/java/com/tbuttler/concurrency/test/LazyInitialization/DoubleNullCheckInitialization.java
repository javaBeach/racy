package com.tbuttler.concurrency.test.LazyInitialization;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.Expect;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.LongResult2;

import com.tbuttler.concurrency.model.LazyInitialization;
import com.tbuttler.concurrency.model.LazyInitialization.ExpensiveObject;

/**
 * These tests don't detect any forbidden outcomes - the initialization checks whether the 
 * instance was already created within a synchronized block.
 * @author Tanja
 *
 */
@JCStressTest
@Description("Uses a method with double null check to lazily initialize an object. The stress tests should fail.")
@Outcome(id = "[5, 5]", expect = Expect.ACCEPTABLE, desc = "ExpensiveObject was initialized by the first actor")
@Outcome(id = "[10, 10]", expect = Expect.ACCEPTABLE, desc = "ExpensiveObject was initialized by the second actor")
@Outcome(id = "[5, 10]", expect = Expect.FORBIDDEN, desc = "Two different objects were intialized")
// According to S. this might be a possibility as well
@Outcome(id = "[0, 10]", expect = Expect.FORBIDDEN, desc = "Actor 1 encountered a null pointer exception.")
@Outcome(id = "[5, 0]", expect = Expect.FORBIDDEN, desc = "Actor 2 encountered a null pointer exception.")
@State
public class DoubleNullCheckInitialization {

	private LazyInitialization initialization = new LazyInitialization();

	@Actor
	public void intializationOne(LongResult2 result) {
		try {
			ExpensiveObject object = initialization.getInstanceWithDoubleNullCheck(5);
			result.r1 = object.getValue();
		} catch (NullPointerException possibility) {
			result.r1 = 0;
		}
	}

	@Actor
	public void concurrentInitialization(LongResult2 result) {
		try {
			ExpensiveObject object = initialization.getInstanceWithDoubleNullCheck(10);
			result.r2 = object.getValue();
		} catch (NullPointerException possibility) {
			result.r2 = 0;
		}
	}

}
