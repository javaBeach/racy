package com.tbuttler.concurrency.testJDK;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Arbiter;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.Expect;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.BooleanResult2;

import java.util.BitSet;

/**
 * Demonstrates word tearing (concurrent modification) when using an unsafe BitSet.
 * Both threads should set their respective bits to true, but test results show that
 * interleaving may occur and threads can write back invalid bits.
 * 
 * Uses an Arbiter to record the results for the testing framework.
 * 
 * @author Tanja
 *
 */
@JCStressTest
@Description("Tests if racy update to BitSet experiences word tearing.")
@Outcome(id = "[true, false]", expect = Expect.FORBIDDEN, desc = "Thread 1 has intervened, word tearing occurred.")
@Outcome(id = "[false, true]", expect = Expect.FORBIDDEN, desc = "Thread 2 has intervened, word tearing occurred.")
@Outcome(id = "[true, true]",  expect = Expect.ACCEPTABLE, desc = "Observed both thread writes.")
@State
public class BitSetTest {

    final BitSet o = new BitSet();
    
    /**
     * Sets the first bit to true.
     */
    @Actor
    public void thread1() {
        o.set(0);
    }

    /**
     * Sets the second bit to true.
     */
    @Actor
    public void thread2() {
        o.set(1);
    }

    /**
     * Reads the values out of the tested BitSet and puts them into a result BooleanResult2 
     * (one of the harnesses accepted result types).
     * @param res
     */
    @Arbiter
    public void observe(BooleanResult2 res) {
        res.r1 = o.get(0);
        res.r2 = o.get(1);
    }

}
