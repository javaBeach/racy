# racy

Racy uses jcstress to show whether some simple examples were implemented in a thread-safe way (or not).

## How JCStress works

JCStress is not a simple unit-testing framework but rather a harness that auto-generates code for testing threads. These tests are executed multiple times, and each time JCStress checks whether the results were the ones to be expected (assuming a thread-safe execution). Unlike unit-tests, the outcomes for each test can vary, i.e., they may or may not result in 
JCStress offers two types of test modes: Mode.Termination and Mode.Continuous.

Mode.Termination expects a single @Actor in the test case. The actor runs a thread that may or may not terminate based on a @Signal. This type of test can be used to detect unsafe ways of interrupting or terminating a thread.
The outcomes to check for can have the IDs TERMINATED (thread terminated, valid outcome) and STALE (thread did not terminate, invalid outcome).

Mode.Continuous expects (how many?) actors. JCStress runs each actor in its own thread. If these threads concurrently modify a data structure, JCStress allows for detecting unsafe modifications by collecting results on (a) which thread made a modification and (b) on what the results of the modifications were (see the compare and swap tests for an example).
The tests may optionally use an @Arbiter to read out whatever modifications were made to a structure and put it into a single outcome.

## Expected, but not found

An important area of testing concurrent code is detecting deadlocks (or livelocks). JCStress has the harness to execute two actors concurrently, multiple times. It also contains a function to detect when threads are not terminating (in Mode.Termination), but I have not yet found a way to combine these two tests.

## Test Results

The tests in this repository were written to compare different implementations. Tests for unsafe implementations will fail (that is not a bug but intentional).

## Requirements

- jdk8 (the library shipped in this repository is not the latest version as that requires jdk9)
- maven
- the libraries in the lib folder need to be in a maven repository
- to run this test run
    - mvn clean install
    - run.bat

## Fork of

- https://travis-ci.org/michaelszymczak/jcstress-bootstrap for providing the JDK8 version of JCStress

## Inspired by

- https://github.com/tmjee/jcstress-tmjee
- http://openjdk.java.net/projects/code-tools/jcstress/
- http://nitschinger.at/Debugging-Concurrency-Issues-with-Open-JDK-Jcstress
- http://zeroturnaround.com/rebellabs/concurrency-torture-testing-your-code-within-the-java-memory-model/

Special thanks to @shipilev for easy to follow explanation: http://shipilev.net/blog/2014/jmm-pragmatics/

