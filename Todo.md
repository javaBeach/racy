## model

1. Use the concurrency libraries for playing around with 

## tests
1. Write documentation for StreamTest: which anomalies could occur?
3. I am missing parameterized tests in the CompareSwap section
4. What does @State indicate?

## documentation

1. put the document structure into the readme
2. point to tests using arbiter, collected results, and mode.termination in the readme file

## expanding & exploring JCStress (ideas)

1. Take a look at the generated test files: how do they work
1. I really would like deadlock detection: extend / overwrite the Processor to create test files 